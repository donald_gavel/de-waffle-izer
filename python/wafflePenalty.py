"""
Actuator penalty technique

    See "Suppressing Anomalous Localized Waffle Behavior in Least
    Squares Wavefront Reconstructors," SPIE 4839, Aug 2002.
"""
import numpy as np

def waffleF(n=16,wrap=False):
    """generate the F matrix for de-wafflizing a spatial
    reconstructor using the penalty function method.
    """
    F = np.zeros((n,n,n,n))
    if not wrap:
        for k in range(n-1):
            for j in range(n-1):
                F[k,j,k,j] = 1
                F[k,j,k,j+1] = -1
                F[k,j,k+1,j] = -1
                F[k,j,k+1,j+1] = 1
    else:
        for k in range(n):
            for j in range(n):
                F[k,j,k,j] = 1
                F[k,j,k,(j+1)%n] = -1
                F[k,j,(k+1)%n,j] = -1
                F[k,j,(k+1)%n,(j+1)%n] = 1        
    F = np.matrix(F.reshape((n*n,n*n)))
    return F

def grad(n=16):
    """create a phase to slopes matrix using finite-differencing
    """
    Hx = np.zeros((n+1,n+1,n,n))
    Hy = np.zeros((n+1,n+1,n,n))
    for i in range(n):
        for j in range(n):
            Hx[i:i+2,j:j+2,i,j] = [[+1,-1],[+1,-1]]
            Hy[i:i+2,j:j+2,i,j] = [[+1,+1],[-1,-1]]
    Hx = np.matrix(Hx.reshape((n+1)*(n+1),n*n))
    Hy = np.matrix(Hy.reshape((n+1)*(n+1),n*n))
    H = np.vstack([Hx,Hy])
    return H

def reconstructor(n=16,wrap=False,alpha=1.):
    """make a reconstructor based on the penalty function method
    """
    H = grad(n)
    F = waffleF(n)
    Q = F.T*F
    R = (H.T*H + alpha*Q).I*H.T
    return R

def simulate(n=16,sd=0.1,alpha=1.,newRandom=True):
    """Example
    n is size of grid
    sd is standard deviation of slope noise
    alpha is the reconstructor waffle penalty parameter
    """
    global phase,noise
    
    if (newRandom):
        p = -11./3.
        a = np.random.normal(size=(n,n))
        fx = np.arange(n) - n/2
        fx,fy = np.meshgrid(fx,fx)
        P = np.sqrt(fx**2 + fy**2)**p
        P[n//2,n//2] = 0
        P = np.fft.fftshift(P)
        a_f = np.fft.fft2(a)
        r_f = np.sqrt(P)*a_f
        r = np.real(np.fft.ifft2(r_f))
        phase = np.matrix(r.reshape(n*n,1))
        
        m = (n+1)*(n+1)*2
        noise = np.matrix(sd*np.random.normal(size=(m))).reshape((m,1))

    H = grad(n=n)
    R = reconstructor(n=n,alpha=alpha)
    s = H*phase + noise
    phase_recon = R*s
    phase_recon = np.array(phase_recon).reshape((n,n))
    phase_original = np.array(phase).reshape((n,n))
    return phase_original, phase_recon
    
def test():
    """compare with and without the de-wafflizing penalty
    """
    p0,pr0 = simulate(alpha=0.,newRandom=True)
    p0,prdw = simulate(alpha=10.,newRandom=False)
    nrows = p0.shape[0]
    border = np.zeros((nrows,1))
    show = np.hstack([p0,border,pr0,border,prdw])
    return show


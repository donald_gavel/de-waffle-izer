;
; Penalty function method in the Fourier domain
;   designed to supress local waffle mode
;
nm = 1.e-9
microns = 1.e-6
cm = 1.e-2
;
n = 128 ; fine grid
na = 48 ; actuators across aperture
alpha = 10.e7;5.e6 ; arbitrary penalty function weighting factor
seed = 5 ; random number generator seed
niter = 25 ; iterations in the reconstructor
iter_gain = .8 ; gain in the iterative reconstructor
dtele = 8.0 ; Gemini telescope diameter
lambda = 1.0*microns ; science wavelength (far-field image)
lambda0 = 0.5*microns ; wavelength where r0 is specified
r00 = 20*cm ; r0
;
spatial_filter = 1
disp_gains = 0
disp_iter = 0
disp_recon = 0
disp_farfield = 1
;
title = ' penalty='+string(alpha,format='(e9.2)')
;
i = complex(0.,1.)
dk = 2*!pi/float(n)
kx = ones(n) ## (findgen(n)-n/2) * dk
ky = transpose(kx)
;
; slope-difference operator
vx = (1/2.)*(exp(i*ky/2.)*(exp(i*kx/2.)-exp(-i*kx/2.)) + exp(-i*ky/2.)*(exp(i*kx/2.)-exp(-i*kx/2.)))
vy = (1/2.)*(exp(i*kx/2.)*(exp(i*ky/2.)-exp(-i*ky/2.)) + exp(-i*kx/2.)*(exp(i*ky/2.)-exp(-i*ky/2.)))
;
; divergence operator
v = kx*vx + ky*vy
;
; global waffle mode, for test purposes
w = cos(n*kx)*cos(n*ky)
fw = ft(w)
;
; local waffle modes, for generating the penalty function
wl = [[1,-1],[-1,1]]
wl = zeropad(wl,n,n)
fwl = ft(wl)
Pw = abs(fwl)^2 ; frequency domain penalty function for local waffle
;
; laplacian operator
A = abs(vx*vx + vy*vy) ; equivalent to the Laplacian in the frequency domain, for discrete spatial samples
;
; generate reconstruction gains
A = A + alpha*Pw ; the regularized Laplacian
fuzz = 1.e-8
mask = real(A le fuzz)
A = A+mask
K = [conj(vx)/A, conj(vy)/A]
;
; display reconstructor matrix results
if (disp_gains) then begin
  disp,abs(K),'K'+title
  disp,abs(vx*K[0:n-1,*]+vy*K[n:2*n-1,*]),'K*Grad'+title
endif
;
; simulate atmospheric phase screen
n2 = 2*n ; oversample
du = dtele/float(na)
ap = circle(n,n,n/2,n/2,na/2,1.)
ap2 = circle(n2,n2,n,n,na,1.)
r0 = r00*(lambda/lambda0)^(6./5.)
kolmog_filter = screengen(n2,n2,r0,du)
phase2 = screengen(kolmog_filter,seed)
phase2_f = ft(phase2)

;   make the Hartmann slope measurements, and mask them on the aperture
if (spatial_filter) then begin
  phase = real(ft(phase2_f[n-n/2:n+n/2-1,n-n/2:n+n/2-1],/inverse))
endif else begin
  phase = rebin(smooth(phase2,2),n,n)
endelse
phase_f = ft(phase)
slope_f = [ft(ft(vx*phase_f,/inverse)*ap),ft(ft(vy*phase_f,/inverse)*ap)]

;   reconstruction with regularization
phase_f_hat = K*slope_f
phase_f_hat = total(reform(phase_f_hat,n,2,n),2)
phase_hat = real(ft(phase_f_hat,/inverse))
phase_err2_save = fltarr(n2,n2,niter+1)
phase_err_save = fltarr(n,n,niter+1)
for iter = 0,niter-1 do begin
  print,'iter = ',strtrim(iter,2) & wait,.01
  phase_err_save[*,*,iter] = (lambda/(2*!pi))*depiston(double(phase - ft(phase_f_hat,/inverse)),ap)/nm
  phase_err2_save[*,*,iter] = (lambda/(2*!pi))*depiston(double(phase2 - rebin(real(ft(phase_f_hat,/inverse)),n2,n2)),ap2)/nm
  phase_err_f = phase_f-phase_f_hat
  slope_err_f = [ft(ft(vx*phase_err_f,/inverse)*ap),ft(ft(vy*phase_err_f,/inverse)*ap)]
  phase_f_hat += iter_gain*total(reform(K*slope_err_f,n,2,n),2)
endfor
phase_err_save[*,*,iter] = (lambda/(2*!pi))*depiston(double(phase - ft(phase_f_hat,/inverse)),ap)/nm
phase_err2_save[*,*,iter] = (lambda/(2*!pi))*depiston(double(phase2 - rebin(real(ft(phase_f_hat,/inverse)),n2,n2)),ap2)/nm
rms_err = fltarr(niter+1)
rms_err2 = fltarr(niter+1)
for iter = 0,niter do begin
  rms_err[iter] = rms(phase_err_save[*,*,iter],ap)
  rms_err2[iter] = rms(phase_err2_save[*,*,iter],ap2)
endfor
if (disp_iter) then begin
  disp,phase_err_save,ap=ap,'phase error iteration'+title
  disp,phase_err2_save,ap=ap2,'phase error 2x iteration'+title
endif
  ;
  p_save = !p
  !p.charsize = 2
  device,window_state = ws
;  if (n_elements(plot_window) eq 0) then plot_window = 0
  plot_window = 0
  if (ws[plot_window] eq 0) then begin
;    window,/free
    window,plot_window
    plot_window = !d.window
    plot,rms_err,ytitle = 'wf error, nm rms',xtitle = 'iteration',/ylog,/nodata,color=100,ticklen=0.5
    plot,rms_err,ytitle = 'wf error, nm rms',xtitle = 'iteration',/ylog,/noerase
    oplot,rms_err2
    p1 = !p & x1 = !x & y1 = !y
  endif else begin
    wset,plot_window
    !p = p1 & !x = x1 & !y = y1
    oplot,rms_err,linestyle=2
    oplot,rms_err2,linestyle=2
  endelse
  !p = p_save
  ;

;
;  create far-field images
far_field = fltarr(n2,n2,niter+1)
for iter = 0,niter do begin
  far_field[*,*,iter] = abs(ft(ap2*exp(i*(2*!pi/lambda)*nm*phase_err2_save[*,*,iter])))
endfor
if (disp_farfield) then begin
  disp,far_field,'far field'+title
endif

;
; display reconstruction results
if (disp_recon) then begin
  disp,phase,'random Kolmogorov phase'
  disp,phase_hat,'reconstructed phase'+title
  disp,depiston(phase-phase_hat,ap),'phase error'+title
endif

end

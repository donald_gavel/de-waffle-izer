;
; zeroPad.pro
;   zero pad an image to a larger size, center the image
;
function zeropad,im,n_arg,m_arg,by=by,no_center=no_center
  s = size(im)
  n0 = s[1]
  m0 = s[2]
  type = s[3]
  if (keyword_set(by)) then begin
    n = n0+n_arg
    m = m0+m_arg
  endif else begin
    n = n_arg
    m = m_arg
  endelse
  if ((m0 gt m) or (n0 gt n)) then begin
    print,'<zeropad> ERROR: output image must be larger than input image'
    return,0
  endif
  case type of
  5: im1 = dblarr(n,m)
  6: im1 = complexarr(n,m)
  9: im1 = dcomplexarr(n,m)
  else: im1 = fltarr(n,m)
  endcase
  im1[0:n0-1,0:m0-1] = im
  if (not keyword_set(no_center)) then im1 = shift(im1,(n-n0)/2,(m-m0)/2)
;  im1[n/2-n0/2:n/2+n0/2-1,m/2-m0/2:m/2+m0/2-1] = im
  return,im1
end

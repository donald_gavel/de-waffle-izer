;+
;   rms.pro - find the rms value of an image
;         ans = rms(im,ap)
;-
function rms,im,ap,warn_nan = warn_nan
	ima = im
	apf = finite(ima)
	nan_ind = where(1-apf,count)
	if (count ne 0) then begin
		ima[nan_ind] = 0
		if (keyword_set(warn_nan)) then print,'<rms> Warning: there are ',strtrim(n_elements(nan_ind),2),' NaNs in the data'
	endif
  	if (n_elements(ap) ne 0) then begin
    	apf *= ap
  	endif
	ima *= apf
	r = sqrt(total(ima*ima)/total(apf))
    return,r
end

; actuatorPenalty.pro
;   compute the control matrix using
;   the actuator penalty method to suppress local waffle
;
;  inputs:
;     H = influence function matrix, arranged as
;           (number of subapertures x 2) by (number of actuators) matrix
;           with all the x-slopes in the first set of rows then all y-slopes in the second set of rows
;     na = number of actuators across the diameter of the aperture (maximum width)
;     ap = a (na) x (na) array of 1s and 0s indicating the active actuators.
;                Use this to define a non-square aperture mask
;
;  optional outputs:
;     ww = the penalty function for localized waffle
;     V = the penalty function for piston tip and tilt
;
;  returns
;     K = the pseudo inverse of H with localized waffle suppressed
;
;
function actuatorPenalty,H,na,ap,ww,V
  s = size(H)
  naa = s[1]
  nsa = s[2]
;  ap = fltarr(na,na)
;  ap[includeacts] = 1
  includeacts = where(ap)
  aindex = intarr(na*na)
  for i=0,naa-1 do aindex[includeacts[i]] = i
  print,'calculating HH'
  HH = transpose(H) ## H
  print,'calculating wstar'
  even = (na/2)*2 eq na
  if (even) then begin
    x = (findgen(na)-na/2) # (fltarr(na)+1)
  endif else begin
    x = (findgen(na)-na/2.+.5) # (fltarr(na)+1)
  endelse
  y = transpose(x)
  u = reform(x,na^2)
  v = reform(y,na^2)
  i = complex(0,1.)
  fop = complexarr(na,na,na^2)
  for uv=0,na^2-1 do begin
    fop[*,*,uv] = exp(2*!pi*i*(x*u[uv]/na+y*v[uv]/na))
  endfor
  fop = reform(fop,na^2,na^2)/(na^2)
  w = fltarr(na,na)
  w[na/2,na/2]=1
  w[na/2-1,na/2]=-1
  w[na/2,na/2-1]=-1
  w[na/2-1,na/2-1]=1
  fw = fop ## reform(w,na^2,1)
  fw = reform(fw,na^2)*na^2
  dfw = complexarr(na^2,na^2)
  for i=0,na^2-1 do dfw[i,i] = fw[i]
  wstar = dfw ## fop
  t = conj(fop) ## wstar
  ww0 = double(transpose(conj(wstar)) ## wstar)
; "compact" ww0
  ww = fltarr(naa,naa)
  for i=0,naa-1 do begin
    for j=0,naa-1 do begin
      ww[i,j] = ww0[includeacts[i],includeacts[j]]
    endfor
  endfor
;  for i=0,naa-1 do begin
;    for j=0,naa-1 do begin
;      ww[i,j] = ww0[aindex[i],aindex[j]]
;    endfor
;  endfor
;  penalize piston, tip, tilt
  print,'calulating piston,tip,tilt penalty'
  piston = reform(ap,na^2)
  piston = piston / sqrt(total(piston^2))
  tip = ap*((findgen(na)-na/2+.5) # (fltarr(na)+1))
  tip = tip / sqrt(total(tip^2))
  tilt = transpose(tip)
  tip = reform(tip,na^2)
  tilt = reform(tilt,na^2)
  V0 = piston # piston + tip # tip + tilt # tilt ;;; allow tip and tilt
  V = fltarr(naa,naa)
  for i=0,naa-1 do begin
    for j=0,naa-1 do begin
      V[i,j] = V0[includeacts[i],includeacts[j]]
    endfor
  endfor
;   solve for K2
  P1 = HH + 100*(ww+V) ;  penalty factor is arbitrary, 100 seems to work well
  print,'solving for K2'
  ludc,P1,index,/double
  K = dblarr(nsa,naa)
  for i=0,nsa-1 do K[i,*] = lusol(P1,index,H[*,i])

  return,K
end
;
; example code (simple square array of actuators and Hartmann subapertures on the Fried geometry
;
na = 10 ; number of actuators across
ns = 11 ; number of Hartmann subapertures
H = fltarr(ns,ns,2,na,na) ; 2 because there are x and y slopes
for i=0,na-1 do begin
  for j=0,na-1 do begin
    H[i,j,0,i,j] = -1
    H[i,j+1,0,i,j] = -1
    H[i+1,j,0,i,j] = 1
    H[i+1,j+1,0,i,j] = 1
    H[i,j,1,i,j] = -1
    H[i,j+1,1,i,j] = 1
    H[i+1,j,1,i,j] = -1
    H[i+1,j+1,1,i,j] = 1
  endfor
endfor
H = transpose(reform(H,ns*ns*2,na*na))
includeActs = fltarr(na*na)+1
ap = fltarr(na,na)+1
K = actuatorPenalty(H,na,ap,ww,V)
end

